```
  .-')                            ('-.        .-') _   .-') _                   _ (`-.   _  .-')                ) (`-.                   
 ( OO ).                        _(  OO)      ( OO ) ) (  OO) )                 ( (OO  ) ( \( -O )                ( OO ).                 
(_)---\_)   ,-.-')   ,--.      (,------. ,--./ ,--,'  /     '._               _.`     \  ,------.   .-'),-----. (_/.  \_)-.   ,--.   ,--.
/    _ |    |  |OO)  |  |.-')   |  .---' |   \ |  |\  |'--...__)             (__...--''  |   /`. ' ( OO'  .-.  ' \  `.'  /     \  `.'  / 
\  :` `.    |  |  \  |  | OO )  |  |     |    \|  | ) '--.  .--'              |  /  | |  |  /  | | /   |  | |  |  \     /\   .-')     /  
 '..`''.)   |  |(_/  |  |`-' | (|  '--.  |  .     |/     |  |                 |  |_.' |  |  |_.' | \_) |  |\|  |   \   \ |  (OO  \   /   
.-._)   \  ,|  |_.' (|  '---.'  |  .--'  |  |\    |      |  |                 |  .___.'  |  .  '.'   \ |  | |  |  .'    \_)  |   /  /\_  
\       / (_|  |     |      |   |  `---. |  | \   |      |  |                 |  |       |  |\  \     `'  '-'  ' /  .'.  \   `-./  /.__) 
 `-----'    `--'     `------'   `------' `--'  `--'      `--'                 `--'       `--' '--'      `-----' '--'   '--'    `--'      
```

# Motivations

This simple tool can be used as a FrontEnd decoy during engagements.

For some edge cases, one would want to keep an access granted to a binded http service.

But you want to have a nice and fancy front-end that mimic the server isn't it ?

So this may be the tool you need.

## Use case #1
Let's say for instance that your compromised server is running a http service in port 8080.
You could change the config of this service to run only on localhost on port 8081, then standard clients will access your service.

```
Standard client (HTTP GET /) =======> +--------------+ ===> redirect on localhost:8081
                                      |              |
                                      |              |
                                      | Silent Proxy |
                                      |              |
You (HTTP GET with secret) =========> +--------------+ ===> redirect on webshell
```


## Use case #2
Let's say for instance that your compromised server is not listening on any port.
A neighbour server is running a http service, so you will redirect all the incomming http traffic into this server.


```
Standard client (HTTP GET /) =======> +--------------+ ===> redirect on server_that_make_sense:80
                                      |              |
                                      |              |
                                      | Silent Proxy |
                                      |              |
You (HTTP GET with secret) =========> +--------------+ ===> redirect on webshell
```



# How it works

This tool is nothing but a golang net/http HanderFunc that can be used as simple as

```golang
	http.HandleFunc("/", HostChangingProxyClosure("https", "google.com", 443, false))
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
```

To access WebShell, a fancy domain generation algorithm is supplied because it is the one used by the mighty CyberBullshit APT (no really, it is based on server time in UTC).

To secure access, you have to sign the server time. Helpers are provided in SignatureHelpers.go to do so. 

# Building

If you are not familiar with go, download a go compiler https://golang.org/dl/ and build a standalone executable.

```bash
go build -o nginx_cgi *.go
```

Of course, you can upx it and do some nice tricks (https://blog.filippo.io/shrink-your-go-binaries-with-this-one-weird-trick/)

# TODO

- [ ] Not hardcode public key for signature checking (maybe a bad idea)

- [ ] Improve http rendering for some weird characters

- [ ] Test it !



