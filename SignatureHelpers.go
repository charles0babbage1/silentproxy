package main

import (
	"crypto/ed25519"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"time"
)

func GetMagicDga() string {
	utcTime := time.Now().UTC()
	magic := fmt.Sprintf("%d%d%d%d", utcTime.Year(), utcTime.Month(), utcTime.Day(), utcTime.Hour())
	return magic
}

func GetVerifyingFunc(b64encodedPublicKey string) func([]byte, []byte) bool {
	pub, err := base64.StdEncoding.DecodeString(b64encodedPublicKey)
	if nil != err {
		return nil
	}
	return func(mess []byte, sig []byte) bool {
		return ed25519.Verify(pub, mess, sig)
	}
}

func GetSigningFunc(b64encodedPrivateKey string) func([]byte) []byte {
	priv, err := base64.StdEncoding.DecodeString(b64encodedPrivateKey)
	if nil != err {
		return nil
	}
	return func(mess []byte) []byte {
		return ed25519.Sign(priv, mess)
	}
}

func test_SignatureHelpers() {
	fmt.Println("Signature Tests")
	pub, priv, err := ed25519.GenerateKey(nil)
	if nil == err {
		fmt.Printf("Priv part = %s\n", base64.StdEncoding.EncodeToString(priv))
		fmt.Printf("Pub part = %s\n", base64.StdEncoding.EncodeToString(pub))
	}
	priv, _ = base64.StdEncoding.DecodeString("gHhTn4RTcra/5A5bfrch+zgsj2x8h4s8VPL3F4dRa9sBgUk0Cd3+EfObEMqgB2RrzNxV5vLckKoZCBsUoNrLPA==")
	pub, _ = base64.StdEncoding.DecodeString("AYFJNAnd/hHzmxDKoAdka8zcVeby3JCqGQgbFKDayzw=")

	verif := GetVerifyingFunc("AYFJNAnd/hHzmxDKoAdka8zcVeby3JCqGQgbFKDayzw=")
	sign := GetSigningFunc("gHhTn4RTcra/5A5bfrch+zgsj2x8h4s8VPL3F4dRa9sBgUk0Cd3+EfObEMqgB2RrzNxV5vLckKoZCBsUoNrLPA==")

	message := []byte(GetMagicDga())
	sig := sign(message)
	fmt.Printf("Signature is %s\n", hex.EncodeToString(sig))
	fmt.Printf("%v\n", verif(message, sig))

}
/*
func main(){
	test_SignatureHelpers()
}
//*/
